#include "utils.h"

/*
Function reverses an array of integers.
Input: array of integers, size of array.
Output: none.
*/
void reverse(int* nums, unsigned int size)
{
	stack* s = new stack;
	unsigned int i = 0;

	initStack(s);
	
	for (i = 0; i < size; i++) // Push all the numbers in the array into the stack
	{
		push(s, nums[i]);
	}

	for (i = 0; i < size; i++) // Pop all the numbers in the stack back into the original array
	{
		nums[i] = pop(s);
	}

	cleanStack(s);
	delete s;
}

/*
Function creates an array of integers from user input and returns it reversed.
Input: none.
Output: integer array.
*/
int* reverse10()
{
	unsigned int i = 0;
	int* arr = new int[SIZE]; // Allocate memory for the array
	
	for (i = 0; i < SIZE; i++) // Get user input for array values
	{
		std::cout << "Please enter an integer value: ";
		std::cin >> arr[i];
	}

	reverse(arr, SIZE); // Call the reverse function to reverse the array

	return arr;
}