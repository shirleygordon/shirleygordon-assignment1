#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>
#define EMPTY -1

/* a linkedList which contains positive integer values. */
typedef struct linkedList
{
	unsigned int num;
	struct linkedList* next;
} linkedList;

void pushList(linkedList** head, unsigned int element);
int popList(linkedList** head); // Return -1 if list is empty

bool isEmpty(linkedList* list);
void cleanList(linkedList** list);

#endif /* LINKEDLIST_H */