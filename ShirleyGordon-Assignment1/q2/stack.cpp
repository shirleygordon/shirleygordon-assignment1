#include "stack.h"

/*
Function adds an element to the top of the stack.
Input: stack pointer, element to add.
Output: none.
*/
void push(stack* s, unsigned int element)
{
	pushList(&(s->list), element);
	s->count++;
}

/*
Function removes an element from the top of the list.
Input: stack pointer.
Output: the element which was removed, or -1 if the list is empty.
*/
int pop(stack* s)
{
	int poppedElement = EMPTY;
	if (!isEmpty(s->list)) // Only call pop function if the list isn't empty
	{
		poppedElement = popList(&(s->list));
		s->count--; // Since an element was popped, count decreases.
	}

	return poppedElement;
}

/*
Function initializes a stack.
Input: stack pointer.
Output: none.
*/
void initStack(stack* s)
{
	s->list = NULL; // List is empty, therefore points to NULL
	s->count = 0;
}

/*
Function cleans the stack and brings it back to its initial state.
Input: stack pointer.
Output: none.
*/
void cleanStack(stack* s)
{
	cleanList(&(s->list)); // First clean the linked list and deallocate memory.
	initStack(s); // Then bring the stack back to its initial state.
}