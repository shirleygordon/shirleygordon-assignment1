#include "linkedList.h"

/*
Function adds an element to the head of the list.
Input: linkedList pointer, element to add.
Output: none.
*/
void pushList(linkedList** head, unsigned int element)
{
	linkedList* newNode = new linkedList; // Allocate memory for the new node

	newNode->num = element; // Put new element into new node

	if (isEmpty(*head)) // If the list is empty, this is the first node. Therefore, it point to NULL.
	{
		newNode->next = NULL;
	}
	else // If list isn't empty
	{
		newNode->next = *(head); // Make the node.next point to the head of the list
	}
	
	*(head) = newNode; // Make the head point to the new node
}

/*
Function removes an element from the head of the list.
Input: linkedList pointer.
Output: the element which was removed, or -1 if the list is empty.
*/
int popList(linkedList** head)
{
	linkedList* temp = *head; // Save head in order to deallocate memory
	int returnValue = EMPTY;

	if (!isEmpty(*head)) // If the list isn't empty
	{
		returnValue = temp->num; // Save the element to be popped in order to return it
		*head = (*head)->next; // Set head to the next node

		delete temp; // Deallocate memory of previous head
	} 

	return returnValue;
}

/*
Function check if a list is empty.
Input: linkedList pointer.
Output: true if list is empty, false otherwise.
*/
bool isEmpty(linkedList* list)
{
	return list == NULL;
}

/*
Function cleans the list.
Input: list pointer.
Output: none.
*/
void cleanList(linkedList** list)
{
	if (!isEmpty(*list)) // list is not empty
	{
		if ((*list)->next != NULL) // end condition
		{
			cleanList(&((*list)->next));
		}

		delete *list;
	}
}