#include "bonus.h"

/*
Function initializes a queue with zeros.
Input: queue pointer, queue size.
Output: none.
*/
void initQueue(bonusQueue* q, unsigned int size)
{
	unsigned int i = 0;

	q->arr = new unsigned int[size]; // Allocate memory for the queue array
	q->size = size; // Save the queue's size in the struct
	q->end = EMPTY; // Nothing enqueued yet
	q->start = 0;
	q->count = 0;

	for (i = 0; i < size; i++) // Initialize each element in the array with 0
	{
		q->arr[i] = 0;
	}
}

/*
Function cleans the queue.
Input: queue pointer.
Output: none.
*/
void cleanQueue(bonusQueue* q)
{
	delete[] q->arr; // Deallocate array memory
	delete q; // Deallocate queue struct memory
}

/*
Function checks if a queue is full.
Input: queue pointer.
Output: true if queue is full, false otherwise.
*/
bool isFull(bonusQueue* q)
{
	bool full = true;

	if (q->count != q->size) // If the last position filled isn't equal to the size of the array (minus 1), it means the queue isn't full.
	{
		full = false;
	}

	return full;
}

/*
Function adds a new value to the end of the array.
Input: queue pointer, new value.
Output: none.
*/
void enqueue(bonusQueue* q, unsigned int newValue)
{
	if (!isFull(q)) // Only enqueue if queue isn't full already
	{
		q->end++; // Update the end index (the new value will be at this index)
		q->arr[q->end % q->size] = newValue; // Add the new value to the array at the end position
		q->count++;
	}
}

/*
Function returns the first element from the queue and "removes" it by changing the start index.
Input: queue pointer.
Output: the first element in the queue, which has been removed from it, or -1 if the queue is empty.
*/
int dequeue(bonusQueue* q)
{
	int returnValue = EMPTY;

	if (q->count != 0) // Queue isn't empty
	{
		returnValue = q->arr[q->start % q->size]; // Save the first element in the queue in order to return it
		q->start++; // Increase the start index
		q->count--;
	}

	return returnValue;
}

/*
Function prints the queue.
Input: queue pointer.
Output: none.
*/
void printQueue(bonusQueue* q)
{
	unsigned int i = 0;

	if (q->count > 0)
	{
		cout << "queue: ";
		for (i = q->start % q->size; i <= q->end % q->size; i++)
		{
			cout << q->arr[i] << " ";
		}
	}
}