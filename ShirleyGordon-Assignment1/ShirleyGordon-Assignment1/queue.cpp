#include "queue.h"

/*
Function initializes a queue with zeros.
Input: queue pointer, queue size.
Output: none.
*/
void initQueue(queue* q, unsigned int size)
{
	unsigned int i = 0;

	q->arr = new unsigned int[size]; // Allocate memory for the queue array
	q->size = size; // Save the queue's size in the struct
	q->lastPosition = EMPTY; // Nothing enqueued yet

	for (i = 0; i < size; i++) // Initialize each element in the array with 0
	{
		q->arr[i] = 0;
	}
}

/*
Function cleans the queue.
Input: queue pointer.
Output: none.
*/
void cleanQueue(queue* q)
{
	delete[] q->arr; // Deallocate array memory
	delete q; // Deallocate queue struct memory
}

/*
Function checks if a queue is full.
Input: queue pointer.
Output: true if queue is full, false otherwise.
*/
bool isFull(queue* q)
{
	bool full = true;
	
	if (q->lastPosition != q->size - 1) // If the last position filled isn't equal to the size of the array (minus 1), it means the queue isn't full.
	{
		full = false;
	}
	
	return full;
}

/*
Function adds a new value to the end of the array.
Input: queue pointer, new value.
Output: none.
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (!isFull(q)) // Only enqueue if queue isn't full already
	{
		if (q->lastPosition == EMPTY) // The queue is empty, therefore we'll put the new value in the first position.
		{
			q->arr[0] = newValue;
			q->lastPosition = 0; // Update the lastPosition variable
		}
		else
		{
			q->arr[q->lastPosition + 1] = newValue; // Put the new value after the last element that's been added to the array.
			q->lastPosition++; // Update the lastPosition variable
		}
	}
}

/*
Function shifts the array to the left (brings each element one position forward).
Input: queue pointer.
Output: none.
*/
void shiftArray(queue* q)
{
	int i = 0;

	for (i = 0; i < q->lastPosition; i++)
	{
		q->arr[i] = q->arr[i + 1];
	}

	q->arr[q->lastPosition] = 0; // Change the element in the lastPosition to 0 since it has been moved forward.
}

/*
Function removes the first element from the queue and returns it.
Input: queue pointer.
Output: the first element in the queue, which has been removed from it, or -1 if the queue is empty.
*/
int dequeue(queue* q)
{
	int returnValue = EMPTY;

	if (q->lastPosition != EMPTY) // If the list isn't empty, dequeue.
	{
		returnValue = q->arr[0]; // Save the first element in order to return it.
		shiftArray(q); // Shift the array to the left to remove the first element.
		q->lastPosition--; // Update the lastPosition variable, since we removed an element.
	}
	return returnValue;
}