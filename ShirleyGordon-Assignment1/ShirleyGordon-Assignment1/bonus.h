#ifndef BONUS_H
#define BONUS_H

using namespace std;

#include <iostream>

#define EMPTY -1

/* a queue contains positive integer values. */
typedef struct bonusQueue
{
	unsigned int* arr;
	unsigned int size;
	int start; // variable stores the index of the first element in queue
	int end; // variable stores the last position in the array that has been filled
	unsigned int count = EMPTY; // variable counts the amount of elements in the array
} bonusQueue;

void initQueue(bonusQueue* q, unsigned int size);
void cleanQueue(bonusQueue* q);

bool isFull(bonusQueue* q);

void enqueue(bonusQueue* q, unsigned int newValue);
int dequeue(bonusQueue* q); // return element in top of queue, or -1 if empty

void printQueue(bonusQueue* q);

#endif /* BONUS_H */