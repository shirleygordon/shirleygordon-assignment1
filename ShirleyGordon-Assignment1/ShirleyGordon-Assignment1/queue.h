#ifndef QUEUE_H
#define QUEUE_H

#define EMPTY -1

/* a queue contains positive integer values. */
typedef struct queue
{
	unsigned int* arr;
	unsigned int size;
	int lastPosition; // variable stores the last position in the array that has been filled
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);

bool isFull(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

#endif /* QUEUE_H */